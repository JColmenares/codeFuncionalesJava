﻿import java.io.IOException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import org.w3c.dom.NodeList;

/* Consumo Funcional de un WebService Soap WSDL que permite sumas dos números*/
public class ConsumoSoap {
	
	public static void main(String[] args) {
		System.out.println(ConsumoSoap.Consulta());
	}
		
	public static String Consulta() {
		
		try {
			//Conexión de con el WebService Soap (Suma de números)
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			//Link del servico sin la anotacion ?wsdl -> Original: http://jmjg.es/apps/soap/service01.php?wsdl
			String url = "http://jmjg.es/apps/soap/service01.php";
			
			//Llamado al metodo que crea la estructura xml de la consulta.
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequestSap(), url);
			
			//Resultado de la consulta
			System.out.println(soapResponse);
			System.out.println("Si de conecto fino esta mierda papa!!!");
			
			//Desarme de respuesta xml
			SOAPBody body = soapResponse.getSOAPBody();
			NodeList code = body.getElementsByTagName("result");
			Node node = (Node) code.item(0);
			String responseCode = node != null ? node.getTextContent() : "";
			
			//Mensaje requerido de la respuesta de la consulta
			System.out.println("Respuesta: " + responseCode);
			
		}catch (Exception ex) {
			System.out.println("Error al consumir soap");
		}
		
		return "Se ejecuto todo el programa";
				
	}
	
	//Metodo encargado de realizar la estructura del xml de la consulta
	public static SOAPMessage createSOAPRequestSap() throws SOAPException, IOException {
		
		//Valor inicial
		SOAPMessage soapMessage = null;
		
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			soapMessage = messageFactory.createMessage();
			SOAPPart soapPart = soapMessage.getSOAPPart();
			
			/*
			 Mensaje a estruturar en xml:
			 
			 <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:urn="urn:service01">
   				<SOAP-ENV:Header/>
   				<SOAP-ENV:Body>
      				<urn:addNumbers>
         				<a>10</a>
         				<b>32</b>
      				</urn:addNumbers>
   				</SOAP-ENV:Body>
			</SOAP-ENV:Envelope>
			  
			*/
			
			/*NOTA: la anotación SOAP-ENV se agrega por defecto en las etiquetas principales 
			  "SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/""(Dicha etiqueta se agrega automaticamnete con su atributo, los otros atributos se deben agregar manualmente)
			  "SOAP-ENV:Header" (Si no se especifica se agrega sola en el esquema)
			  "SOAP-ENV:Body" (Para este caso se va estructurando para enviar un mensaje)
			  
			*/
			
			//Estructura de Soap envelope
			//Se agrega otros atributos a la etiqueta "SOAP-ENV:Envelope"
			SOAPEnvelope envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
			envelope.addNamespaceDeclaration("urn", "urn:service01");
			
			//Estructura de Soap Body
			//Dichas etiquetas se agregan de izquierda a derecha vista inicialmente de esta manera urn:addNumbers en el codigo se coloca al revés
			SOAPBody soapBody = envelope.getBody();
			SOAPElement soapBodyElem = soapBody.addChildElement("addNumbers", "urn");
			
			
			//Se agregan etiquetas con su valor
			SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("a");
			soapBodyElem1.addTextNode("100");
			
			SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("b");
			soapBodyElem2.addTextNode("483");
			
			//Se guarda la estructura creada
			soapMessage.saveChanges();
			
			System.out.println(soapMessage);
			
		} catch (Exception e) {
			System.out.println("No se creo el xml");
		}
		
		// Se chequea la salida del xml
		System.out.println("Request SOAP Message = ");
		soapMessage.writeTo(System.out);
		System.out.println();
		return soapMessage;
	}
	
	
}
